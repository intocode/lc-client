module.exports = {
  extends: [
    'react-app', 
    // 'arbnb', 
    'prettier'
  ],
  rules: {
    // делаем error, чтобы зачищать консоли, если где-то консоль важна,
    // то правило нужно отключить на строке
    'no-console': 'error',

    // // иначе компонента выглядят уродски
    'arrow-body-style': 'off',

    // отключаем из-за использования immer в редьюсерах
    // https://redux-toolkit.js.org/usage/immer-reducers#linting-state-mutations
    'no-param-reassign': [
      'error',
      { props: true, ignorePropertyModificationsFor: ['state'] },
    ],

    'no-underscore-dangle': ['error', { allow: ['_id'] }],

    // свойство появилось в cra@5
    'react/function-component-definition': 'off',
  },
  overrides: [
    {
      files: ['**/*.stories.*'],
      rules: {
        'import/no-anonymous-default-export': 'off',
        'react/jsx-props-no-spreading': 'off',
      },
    },
  ],
};
