import {
  CircleVideo,
  CircleVideoProps,
} from "../../components/CircleVideo/CircleVideo";
import { Meta } from "@storybook/react";

export default {
  title: "CircleVideo",
  component: CircleVideo,
} as Meta;

export const Default = (args: CircleVideoProps) => <CircleVideo {...args} />;
