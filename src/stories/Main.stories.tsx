import { Main } from '../components/layout/Main/Main';

export default {
  title: 'Main',
  component: Main,
};

const Template = () => {
  return <Main />;
};

export const Default = Template;
