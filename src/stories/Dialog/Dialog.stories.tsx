import { Dialog, DialogProps } from "../../components/Dialog/Dialog";
import { Meta } from "@storybook/react";

export default {
  title: "Dialog",
  component: Dialog,
} as Meta;

export const Default = (args: DialogProps) => <Dialog {...args} />;
