import { Button, ButtonProps } from "../../components/Button/Button";
import { Meta } from "@storybook/react";

export default {
  title: "Button",
  component: Button,
} as Meta;

export const Default = (args: ButtonProps) => (
  <Button {...args}>Click me</Button>
);
