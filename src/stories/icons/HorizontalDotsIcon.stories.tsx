import HorizontalDotsIcon from '../../components/icons/HorizontalDotsIcon';

export default {
  title: 'HorizontalDotsIcon',
  component: HorizontalDotsIcon,
};

const Template = () => {
  return <HorizontalDotsIcon />;
};

export const Default = Template;
