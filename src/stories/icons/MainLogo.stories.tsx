import MainLogo from '../../components/icons/MainLogo';

export default {
  title: 'MainLogo',
  component: MainLogo,
};

const Template = () => {
  return <MainLogo />;
};

export const Default = Template;
