import VerticalDotsIcon from '../../components/icons/VerticalDotsIcon';

export default {
  title: 'VerticalDotsIcon',
  component: VerticalDotsIcon,
};

const Template = () => {
  return <VerticalDotsIcon />;
};

export const Default = Template;
