import ThemeIcon from '../../components/icons/ThemeIcon';

export default {
  title: 'ThemeIcon',
  component: ThemeIcon,
};

const Template = () => {
  return <ThemeIcon />;
};

export const Default = Template;
