import { Header } from '../components/layout/Header';

export default {
  title: 'Header',
  component: Header,
};

const Template = () => {
  return <Header />;
};

export const Default = Template;
