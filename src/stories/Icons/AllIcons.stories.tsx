import {
  InterviewEndIcon,
  InterviewEndIconProps,
} from "../../components/Icons/InterviewEndIcon";
import {
  RunCodeIcon,
  RunCodeIconProps,
} from "../../components/Icons/RunCodeIcon";
import {
  TemplateEditIcon,
  TemplateEditIconProps,
} from "../../components/Icons/TemplateEditIcon";
import {
  VideoOffIcon,
  VideoOffIconProps,
} from "../../components/Icons/VideoOffIcon";
import {
  ModalCloseIcon,
  ModalCloseIconProps,
} from "../../components/Icons/ModalCloseIcon";
import { Meta } from "@storybook/react";

export default {
  title: "Icon",
  subcomponents: {
    InterviewEndIcon,
    RunCodeIcon,
    TemplateEditIcon,
    VideoOffIcon,
    ModalCloseIcon,
  },
} as Meta;

// export const AllIconsStory = (args: InterviewEndIconProps) => (
//   <>
//     <InterviewEndIcon {...args} />
//     <RunCodeIcon {...args} />
//     <TemplateEditIcon {...args} />
//     <VideoOffIcon {...args} />
//   </>
// );

export const InterviewEndIconStory = (args: InterviewEndIconProps) => (
  <InterviewEndIcon {...args} />
);

export const RunCodeIconStory = (args: RunCodeIconProps) => (
  <RunCodeIcon {...args} />
);

export const TemplateEditIconStory = (args: TemplateEditIconProps) => (
  <TemplateEditIcon {...args} />
);

export const VideoOffIconStory = (args: VideoOffIconProps) => (
  <VideoOffIcon {...args} />
);

export const ModalCloseIconStory = (args: ModalCloseIconProps) => {
  <ModalCloseIcon {...args} />;
};
