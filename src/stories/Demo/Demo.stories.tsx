import { Demo, DemoProps } from '../../components/Demo/Demo';
import { Meta } from '@storybook/react';

export default {
  title: 'Demo',
  component: Demo,
} as Meta;

export const Default = (args: DemoProps) => <Demo {...args}>Click me</Demo>;
