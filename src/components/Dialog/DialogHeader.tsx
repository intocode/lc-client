import React from "react";
import { ModalCloseIcon } from "../Icons/ModalCloseIcon";

export type DialogHeaderProps = {
  snippenName: string | undefined;
  onModalClose: () => void;
};

export const DialogHeader = ({
  snippenName,
  onModalClose,
}: DialogHeaderProps) => {
  return (
    <div className="flex justify-between items-center px-5 py-5 border-b">
      <h1 className="font-bold text-sm">{snippenName}</h1>
      <ModalCloseIcon
        className="cursor-pointer"
        width="12"
        height="12"
        onClick={onModalClose}
      />
    </div>
  );
};
