import React, { useState } from "react";

export type SnippetExampleProps = {
  exampleOneInput: string | undefined;
  exampleOneOutput: string | undefined;
  exampleTwoInput: string | undefined;
  exampleTwoOutput: string | undefined;
};

export const SnippetExample = ({
  exampleOneInput,
  exampleOneOutput,
  exampleTwoInput,
  exampleTwoOutput,
}: SnippetExampleProps) => {
  return (
    <div className="px-5">
      <h1 className="font-bold py-5">Example 1:</h1>
      <ul>
        <li className="px-5">
          <b>Input:</b> {exampleOneInput}
        </li>
        <li className="px-5">
          <b>Output:</b> {exampleOneOutput}
        </li>
      </ul>
      <h1 className="font-bold py-5">Пример 2:</h1>
      <ul>
        <li className="px-5">
          <b>Input:</b> {exampleTwoInput}
        </li>
        <li className="px-5">
          <b>Output:</b> {exampleTwoOutput}
        </li>
      </ul>
    </div>
  );
};
