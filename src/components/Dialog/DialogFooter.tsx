import React from "react";
import { Button } from "../Button/Button";

export type DialogFooterProps = {
  difficulty: string | undefined;
  onModalClose: () => void;
};

export const DialogFooter = ({
  difficulty,
  onModalClose,
}: DialogFooterProps) => {
  return (
    <div className="flex items-center justify-between px-5 py-5 border-t">
      <div className="text-green-500 font-bold">{difficulty}</div>
      <div>
        <Button
          className="text-blue-500"
          children="Закрыть"
          onClick={onModalClose}
        />
        <Button
          className="bg-blue-500 text-white shadow-md"
          children="Выбрать"
          onClick={onModalClose}
        />
      </div>
    </div>
  );
};
