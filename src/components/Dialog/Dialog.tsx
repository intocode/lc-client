import React from "react";
import { DialogHeader } from "./DialogHeader";
import { DialogFooter } from "./DialogFooter";
import { SnippetExample } from "./SnippetExample";

export type DialogProps = {
  snippetName: string | undefined;
  snippetGoal: string | undefined;
  snippetDesc: string | undefined;
  snippetDifficulty: string | undefined;
  exampleOneInput: string | undefined;
  exampleOneOutput: string | undefined;
  exampleTwoInput: string | undefined;
  exampleTwoOutput: string | undefined;
  onModalClose: () => void;
};

export const Dialog = ({
  snippetName,
  snippetGoal,
  snippetDesc,
  snippetDifficulty,
  exampleOneInput,
  exampleOneOutput,
  exampleTwoInput,
  exampleTwoOutput,
  onModalClose,
}: DialogProps) => {
  return (
    <div className="bg-black/30 h-screen absolute flex justify-center items-center inset-0">
      <div className="w-2/4 rounded-md shadow-2xl bg-white">
        <DialogHeader onModalClose={onModalClose} snippenName={snippetName} />
        <p className="px-5 py-5">{snippetGoal}</p>
        <SnippetExample
          exampleOneInput={exampleOneInput}
          exampleOneOutput={exampleOneOutput}
          exampleTwoInput={exampleTwoInput}
          exampleTwoOutput={exampleTwoOutput}
        />
        <p className="px-5 py-5">{snippetDesc}</p>
        <DialogFooter
          onModalClose={onModalClose}
          difficulty={snippetDifficulty}
        />
      </div>
    </div>
  );
};
