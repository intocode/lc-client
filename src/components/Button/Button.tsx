import React from "react";

export type ButtonProps = React.HTMLAttributes<HTMLButtonElement> & {
  className: string;
};

export const Button = ({ className, ...props }: ButtonProps) => {
  return <button className={`rounded-sm px-5 py-2 ${className}`} {...props} />;
};
