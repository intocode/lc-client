import SplitPane from 'react-split-pane';
import restartIcon from '../../assets/restart.svg';

function RightBlock() {
  return (
    <SplitPane
      split="horizontal"
      minSize={150}
      defaultSize={'83%'}
      maxSize={-100}
    >
      <div className="flex-1 flex flex-col">
        <div className="flex justify-between py-3 text-sm leading-snug bg-gray-100">
          <div className="flex justify-center">Решение</div>
          <div className="flex justify-center mr-6">
            <img src={restartIcon} alt="vector" />
          </div>
        </div>
        <div className="border-y border-l border-solid border-gray-200 flex-1">
          <div className="pr-6 my-3 mx-4 flex-1"></div>
        </div>
      </div>
      <div className="text-gray-400 border border-solid border-gray-200 h-full py-1 px-4">
        Консоль очищена
      </div>
    </SplitPane>
  );
}

export default RightBlock;
