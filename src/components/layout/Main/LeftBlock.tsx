function LeftBlock() {
  return (
    <div className="flex flex-col h-full">
      <div className="flex text-sm leading-snug bg-gray-100">
        <div className="flex justify-center w-40 border-r border-solid border-gray-200 py-3 bg-white">
          Задача
        </div>
        <div className="flex justify-center w-40 py-3">Сниппеты</div>
      </div>
      <div className="border-y border-r border-solid border-gray-200 flex-1">
        <div className="pl-6 my-3 mt-4"></div>
      </div>
    </div>
  );
}

export default LeftBlock;
