import SplitPane from 'react-split-pane';
import LeftBlock from './LeftBlock';
import RightBlock from './RightBlock';
import './split-pane.css';

export const Main = () => {
  return (
    <SplitPane
      className="border-t border-solid border-gray-200"
      split="vertical"
      minSize={150}
      defaultSize={'50%'}
      maxSize={-200}
    >
      <LeftBlock />
      <RightBlock />
    </SplitPane>
  );
};
