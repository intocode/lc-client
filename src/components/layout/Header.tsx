import Logo from '../icons/MainLogo';
import ThemeIcon from '../icons/ThemeIcon';

export const Header = () => {
  return (
    <header className="container py-3 px-6">
      <div className="flex justify-between text-blue-500">
        <div className="flex items-center space-x-12">
          <Logo />
          <ul className="flex space-x-12">
            <li>Заготовки</li>
            <li>Архив</li>
          </ul>
        </div>
        <div className="flex items-center space-x-10 ">
          <ThemeIcon />
          <div>Войти</div>
        </div>
      </div>
    </header>
  );
};
