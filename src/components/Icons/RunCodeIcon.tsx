import React from "react";

export type RunCodeIconProps = React.SVGAttributes<SVGElement> & {
  className?: string;
  width?: string;
  height?: string;
};

export const RunCodeIcon = ({
  className,
  width = "16",
  height = "16",
}: RunCodeIconProps) => {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      viewBox="0 0 10 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g>
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M9.02122 4.38916C9.12899 4.45103 9.21853 4.54024 9.2808 4.64778C9.34307 4.75532 9.37585 4.87739 9.37585 5.00166C9.37585 5.12593 9.34307 5.24799 9.2808 5.35554C9.21853 5.46308 9.12899 5.55229 9.02122 5.61416L1.72934 9.89978C1.24309 10.1854 0.624969 9.84228 0.624969 9.28728L0.624969 0.715408C0.624969 0.159783 1.24372 -0.182717 1.72934 0.102908L9.02122 4.38916Z"
          fill="white"
        />
      </g>
    </svg>
  );
};
