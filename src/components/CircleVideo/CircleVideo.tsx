import React from "react";

export type CircleVideoProps = React.HTMLAttributes<HTMLImageElement> & {
  path: string;
  width: string;
  height: string;
  alt?: string;
};

export const CircleVideo = ({
  alt = "avatar",
  path,
  ...props
}: CircleVideoProps) => {
  return <img src={path} alt={alt} {...props} />;
};
