import React from 'react';

export type DemoProps = {
  children: React.ReactNode;
  onClick: () => void;
};

export const Demo = ({ children, onClick }: DemoProps) => {
  return (
    <button
      className="bg-purple-500 text-white px-6 py-2 rounded-sm shadow-md"
      onClick={onClick}
    >
      {children}
    </button>
  );
};
