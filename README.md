# Live Coding Project

_Приложение для проведения технических собеседований. Есть возможность лайв-кодинга и видеосвязи._

⚠️ _Проект находитя в стадии разработки. MVP будет запущен в начале марта._

## Solution Stack

- [TypeScript](https://github.com/microsoft/TypeScript)
- [React](https://github.com/facebook/react)
- [Redux Toolkit](https://github.com/reduxjs/redux-toolkit)
- [Tailwind CSS](https://github.com/tailwindlabs/tailwindcss)
- [Storybook](https://github.com/storybookjs/storybook)
- [Socket.io](https://github.com/socketio/socket.io)

## Установка и запуск

📦 В качестве пакетного менеджера используется [Yarn](https://yarnpkg.com/).

Для локального развёртывания приложения:

```
git clone https://bitbucket.org/intocode/lc-client.git
cd lc-client && yarn
```

Запуск в режиме разработки:

```
yarn start
```

Запуск Storybook:

```
yarn storybook
```

Чекинг перед коммитом:

```
yarn run check
```

## Deployment & Production

Серверная часть проекта находится [здесь](https://bitbucket.org/intocode/lc-server).

---

⚙️ _Тестовый деплой будет готов в начале февраля._

---
